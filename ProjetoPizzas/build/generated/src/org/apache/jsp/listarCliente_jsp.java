package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.List;
import br.com.projetopizzas.model.Pessoa;

public final class listarCliente_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\">\n");
      out.write("    <head>\n");
      out.write("        <title>Bootstrap Example</title>\n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("        <link href=\"bootstrap3/bootstrap.min.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("        <script src=\"bootstrap3/bootstrap.min.js    \" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"bootstrap3/jquery.min.js\" type=\"text/javascript\"></script>\n");
      out.write("        <link href=\"bootstrap3/css.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Roboto\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Montserrat\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\">  \n");
      out.write("        <link href=\"css.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("        <style>\n");
      out.write("            /* Set height of the grid so .sidenav can be 100% (adjust as needed) */\n");
      out.write("            .row.content {height: 550px}\n");
      out.write("\n");
      out.write("            /* Set gray background color and 100% height */\n");
      out.write("            .sidenav {\n");
      out.write("                background-color: #f1f1f1;\n");
      out.write("                height: 100%;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            /* On small screens, set height to 'auto' for the grid */\n");
      out.write("            @media screen and (max-width: 767px) {\n");
      out.write("                .row.content {height: auto;} \n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("        <script>\n");
      out.write("            function mascara(i) {\n");
      out.write("\n");
      out.write("                var v = i.value;\n");
      out.write("\n");
      out.write("                if (isNaN(v[v.length - 1])) { // impede entrar outro caractere que não seja número\n");
      out.write("                    i.value = v.substring(0, v.length - 1);\n");
      out.write("                    return;\n");
      out.write("                }\n");
      out.write("\n");
      out.write("                i.setAttribute(\"maxlength\", \"14\");\n");
      out.write("                if (v.length == 3 || v.length == 7)\n");
      out.write("                    i.value += \".\";\n");
      out.write("                if (v.length == 11)\n");
      out.write("                    i.value += \"-\";\n");
      out.write("\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            /* Máscaras ER */\n");
      out.write("            function mascaratelefone(o, f) {\n");
      out.write("                v_obj = o\n");
      out.write("                v_fun = f\n");
      out.write("                setTimeout(\"execmascaratelefone()\", 1)\n");
      out.write("            }\n");
      out.write("            function execmascaratelefone() {\n");
      out.write("                v_obj.value = v_fun(v_obj.value)\n");
      out.write("            }\n");
      out.write("            function mtel(v) {\n");
      out.write("                v = v.replace(/\\D/g, \"\"); //Remove tudo o que não é dígito\n");
      out.write("                v = v.replace(/^(\\d{2})(\\d)/g, \"($1) $2\"); //Coloca parênteses em volta dos dois primeiros dígitos\n");
      out.write("                v = v.replace(/(\\d)(\\d{4})$/, \"$1-$2\"); //Coloca hífen entre o quarto e o quinto dígitos\n");
      out.write("                return v;\n");
      out.write("            }\n");
      out.write("            function id(el) {\n");
      out.write("                return document.getElementById(el);\n");
      out.write("            }\n");
      out.write("            window.onload = function () {\n");
      out.write("                id('telefone').onkeyup = function () {\n");
      out.write("                    mascaratelefone(this, mtel);\n");
      out.write("                }\n");
      out.write("            }\n");
      out.write("        </script>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <form name=\"cadastrarPessoa\" action=\"CadastrarPessoa\" method=\"post\">\n");
      out.write("            <nav class=\"navbar navbar-inverse visible-xs\">\n");
      out.write("                <div class=\"container-fluid\">\n");
      out.write("                    <div class=\"navbar-header\">\n");
      out.write("                        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#myNavbar\">\n");
      out.write("                            <span class=\"icon-bar\"></span>\n");
      out.write("                            <span class=\"icon-bar\"></span>\n");
      out.write("                            <span class=\"icon-bar\"></span>                        \n");
      out.write("                        </button>\n");
      out.write("                        <a class=\"navbar-brand\" href=\"#\">Logo</a>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"collapse navbar-collapse\" id=\"myNavbar\">\n");
      out.write("                        <ul class=\"nav navbar-nav\">\n");
      out.write("                            <li class=\"active\"><a href=\"#\">Dashboard</a></li>\n");
      out.write("                            <li><a href=\"#\">Age</a></li>\n");
      out.write("                            <li><a href=\"#\">Gender</a></li>\n");
      out.write("                            <li><a href=\"#\">Geo</a></li>\n");
      out.write("                        </ul>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </nav>\n");
      out.write("\n");
      out.write("            <div class=\"container-fluid\">\n");
      out.write("                <div class=\"row content\">\n");
      out.write("                    <div class=\"col-sm-3 sidenav hidden-xs\">\n");
      out.write("                        <h2>\n");
      out.write("                            <img id=\"logo\" src=\"img/logo.jpg\" alt=\"\"/>\n");
      out.write("                        </h2>\n");
      out.write("                        <ul class=\"nav nav-pills nav-stacked\">\n");
      out.write("                            <li><a href=\"dashboard.jsp\">Dashboard</a></li>\n");
      out.write("                            <li><a href=\"cadastrarCliente.jsp\">Cadastro de Clientes</a></li>\n");
      out.write("                            <li class=\"active\"><a href=\"#\">Listar Clientes</a></li>\n");
      out.write("                            <li><a href=\"lanches.jsp\">Cadastrar Produtos</a></li>\n");
      out.write("                            <li><a href=\"lanches.jsp\">Listar Produtos</a></li>\n");
      out.write("                        </ul><br>\n");
      out.write("                    </div>\n");
      out.write("                    <br>\n");
      out.write("                    <table align=\"center\" border=\"1\">\n");
      out.write("                        <tr>\n");
      out.write("                            <td colspan=\"5\" align=\"center\">Lista de Clientes</td>\n");
      out.write("                        </tr>\n");
      out.write("                        <tr>\n");
      out.write("                            <th align=\"center\">Nome:</th>\n");
      out.write("                            <th align=\"center\">CPF:</th>\n");
      out.write("                            <th align=\"center\">Telefone:</th>\n");
      out.write("                            <th align=\"center\">Email</th>\n");
      out.write("                            <th align=\"center\" colspan=\"2\">Editar</th>\n");
      out.write("                        </tr>\n");
      out.write("                        ");

                            List<Pessoa> pessoas = (List<Pessoa>) request.getAttribute("pessoas");
                            for (Pessoa pessoa : pessoas) {
                        
      out.write("\n");
      out.write("                        <tr>\n");
      out.write("                            <td align=\"center\">");
      out.print(pessoa.getNomePessoa());
      out.write("</td>\n");
      out.write("                            <td align=\"center\">");
      out.print(pessoa.getCpfPessoa());
      out.write("</td>\n");
      out.write("                            <td align=\"center\">");
      out.print(pessoa.getTelefonePessoa());
      out.write("</td>\n");
      out.write("                            <td align=\"center\">");
      out.print(pessoa.getEmailPessoa());
      out.write("</td>\n");
      out.write("                            <td align=\"center\"><a href=\"ExcluirProduto?idProduto=");
      out.print(pessoa.getNomePessoa());
      out.write("\">Excluir</td>\n");
      out.write("                            <td align=\"center\"><a href=\"CarregarProduto?idProduto=");
      out.print(pessoa.getNomePessoa());
      out.write("\">Alterar</td>\n");
      out.write("                        </tr>\n");
      out.write("                        ");

                            }
                        
      out.write("\n");
      out.write("                    </table>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </form>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
