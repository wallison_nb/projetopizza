CREATE table pessoa(
	idPessoa serial not null,
	nomePessoa VARCHAR(100),
	cpfPessoa CHAR(11),
	telefonePessoa VARCHAR(11),
	emailPessoa VARCHAR(50),
	senhaPessoa VARCHAR(50),
	constraint pk_pessoa PRIMARY KEY(idPessoa)
);

CREATE TABLE produto(
	idproduto serial not null,
	descproduto varchar(40),
	marcaproduto varchar(40),
	modeloproduto varchar(40),
	valorproduto numeric(9,2),
	constraint pk_produto primary key(idproduto)
);