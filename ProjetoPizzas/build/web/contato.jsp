<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="bootstrap3/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap3/bootstrap.min.js    " type="text/javascript"></script>
        <script src="bootstrap3/jquery.min.js" type="text/javascript"></script>
        <link href="bootstrap3/css.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">   
        <style>
            /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
            .row.content {height: 550px}

            /* Set gray background color and 100% height */
            .sidenav {
                background-color: #f1f1f1;
                height: 100%;
            }

            /* On small screens, set height to 'auto' for the grid */
            @media screen and (max-width: 767px) {
                .row.content {height: auto;} 
            }
        </style>
    </head>
    <body>

        <nav class="navbar navbar-inverse visible-xs">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#">Logo</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Dashboard</a></li>
                        <li><a href="#">Age</a></li>
                        <li><a href="#">Gender</a></li>
                        <li><a href="#">Geo</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row content">
                <div class="col-sm-3 sidenav hidden-xs">
                    <h2>
                        <img id="logo" src="img/logo.jpg" alt=""/>
                    </h2>
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="index.jsp">Pizzas</a></li>
                        <li><a href="lanches.jsp">Lanches</a></li>
                        <li><a href="doce.jsp">Pizzas Doces</a></li>
                        <li class="active"><a href="#section3">Contato</a></li>
                        <li><a href="login.jsp">Entrar</a></li>
                    </ul><br>
                </div>
                <br>

                <div class="col-sm-5">
                    <div class="well">
                        <h4 class="texto">Contato</h4>
                        <div class="row">
                            <div class="col-sm-5">
                                <h3>Nome:</h3>
                                <p><input class="w3-input w3-border" type="text" placeholder="Nome" name="Name" required></p>
                            </div>

                            <div class="col-sm-7">
                                <h3>Email:</h3>
                                <p><input class="w3-input w3-border" type="text" placeholder="Email" name="Email" required></p>
                            </div>

                            <div class="col-sm-12">
                                <h3>Mensagem:</h3>
                                <p><input class="w3-input w3-border" type="text" placeholder="Mensagem" name="Message" required></p>
                            </div>
                            <button type="submit" class="w3-button w3-block w3-black">Enviar</button>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="well">
                        <h4 class="texto">Pizzaria</h4>
                        <div class="row">
                            <p><i class="fa fa-fw fa-map-marker"></i> Bella Massa</p>
                            <p><i class="fa fa-fw fa-phone"></i>(17) 3498-6547</p>
                            <p><i class="fa fa-fw fa-envelope"></i> belaMassa@gmail.com</p>
                            <h4>N�s Aceitamos</h4>
                            <p><i class="fa fa-fw fa-cc-amex"></i> D�bito</p>
                            <p><i class="fa fa-fw fa-credit-card"></i> Cart�o de Credito</p>
                            <br>
                            <i class="fa fa-facebook-official w3-hover-opacity w3-large"></i>
                            <i class="fa fa-instagram w3-hover-opacity w3-large"></i>
                            <i class="fa fa-snapchat w3-hover-opacity w3-large"></i>
                            <i class="fa fa-pinterest-p w3-hover-opacity w3-large"></i>
                            <i class="fa fa-twitter w3-hover-opacity w3-large"></i>
                            <i class="fa fa-linkedin w3-hover-opacity w3-large"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
