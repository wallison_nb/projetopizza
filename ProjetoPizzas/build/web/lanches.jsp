<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="bootstrap3/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap3/bootstrap.min.js    " type="text/javascript"></script>
        <script src="bootstrap3/jquery.min.js" type="text/javascript"></script>
        <link href="bootstrap3/css.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        
        
         <link href="bootstrap3/slideshow.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap3/slideshow.js" type="text/javascript"></script>
        <style>
            /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
            .row.content {height: 550px}

            /* Set gray background color and 100% height */
            .sidenav {
                background-color: #f1f1f1;
                height: 100%;
            }

            /* On small screens, set height to 'auto' for the grid */
            @media screen and (max-width: 767px) {
                .row.content {height: auto;} 
            }
        </style>
    </head>
    <body>

        <nav class="navbar navbar-inverse visible-xs">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#">Logo</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Dashboard</a></li>
                        <li><a href="#">Age</a></li>
                        <li><a href="#">Gender</a></li>
                        <li><a href="contato.html">Contato</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row content">
                <div class="col-sm-3 sidenav hidden-xs">
                    <h2>
                        <img id="logo" src="img/logo.jpg" alt=""/>
                    </h2>
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="index.jsp">Pizzas</a></li>
                        <li class="active"><a href="#">Lanches</a></li>
                        <li><a href="doce.jsp">Pizzas Doces</a></li>
                        <li><a href="contato.jsp">Contato</a></li>
                        <li><a href="login.jsp">Entrar</a></li>
                    </ul><br>
                </div>
                <br>

                <div class="col-sm-9">
                    <div class="well">
                       <div class="container">
                           <h2 class="texto">Todos os Lanches</h2>  
                            <div class="w3-content w3-display-container">
                                <img class="mySlides" src="img/lanche2.jpg" style="width:100%">
                                <img class="mySlides" src="img/lanche3.png" style="width:100%">
                                <img class="mySlides" src="img/lanche4.png" style="width:100%">

                                <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
                                <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
                            </div>
                            <script>
                                var slideIndex = 1;
                                showDivs(slideIndex);

                                function plusDivs(n) {
                                    showDivs(slideIndex += n);
                                }

                                function showDivs(n) {
                                    var i;
                                    var x = document.getElementsByClassName("mySlides");
                                    if (n > x.length) {
                                        slideIndex = 1
                                    }
                                    if (n < 1) {
                                        slideIndex = x.length
                                    }
                                    for (i = 0; i < x.length; i++) {
                                        x[i].style.display = "none";
                                    }
                                    x[slideIndex - 1].style.display = "block";
                                }
                            </script>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/x-burger.png" alt=""/>
                                <h4>X-MODA:</h4>
                                <p>Mussarela, hamb�rguer, presunto, frango, bacon, tomate, molho e or�gano // Opcional: fechada ou aberta</p>
                                <h4>R$ 15,00</h4>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/x-moda.png" alt=""/>
                                <h4>X-EGG:</h4>
                                <p>Mussarela, hamb�rguer, presunto, ovo, tomate, alface, molho e or�gano // Opcional: fechada ou aberta</p>
                                <h4>R$ 15,00</h4>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/lanche1.png" alt=""/>
                                <h4>X-Burger Dog:</h4>
                                <p>Mussarela, hamb�rguer, presunto, salsicha, tomate, alface, molho e or�gano // Opcional: fechada ou aberta</p>
                                <h4>R$ 15,00</h4>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/lanche5.png" alt=""/>
                                <h4>X-Bacon:</h4>
                                <p>Mussarela, hamb�rguer, presunto, bacon, tomate, alface, molho e or�gano // Opcional: fechada ou aberta</p>
                                <h4>R$ 15,00</h4>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/lanche4.png" alt=""/>
                                <h4>X-BURGER:</h4>
                                <p>Mussarela, hamb�rguer, presunto, tomate, alface, molho e or�gano // Opcional: fechada ou aberta</p>
                                <h4>R$ 15,00</h4>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
