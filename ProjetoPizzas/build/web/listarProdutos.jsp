<%@page import="br.com.projetopizzas.model.Produto"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="bootstrap3/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap3/bootstrap.min.js    " type="text/javascript"></script>
        <script src="bootstrap3/jquery.min.js" type="text/javascript"></script>
        <link href="bootstrap3/css.css" rel="stylesheet" type="text/css"/>
        <link href="bootstrap3/slideshow.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap3/slideshow.js" type="text/javascript"></script>
    </head>
    <body>
        <nav class="navbar navbar-inverse visible-xs">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#">Logo</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Dashboard</a></li>
                        <li><a href="#">Age</a></li>
                        <li><a href="#">Gender</a></li>
                        <li><a href="contato.jsp">Contato</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row content">
                <div class="col-sm-3 sidenav hidden-xs">
                    <h2>
                        <img id="logo" src="img/logo.jpg" alt=""/>
                    </h2>
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="dashboard.jsp">Dashboard</a></li>
                        <li><a href="cadastrarProduto.jsp">Cadastrar Produtos</a></li>
                        <li class="active"><a href="ListarProduto">Listar Produtos</a></li>
                    </ul><br>
                </div>
                <br>

                <div class="col-sm-9">
                    <div class="well">
                        <div class="container">
                            <h2 class="texto">Lista de Produtos</h2>  
                            
                                <table align="center" border="1">
                                    <tr>
                                        <th align="center">Id Produto</th>
                                        <th align="center">Descri��o</th>
                                        <th align="center">Marca</th>
                                        <th align="center">Modelo</th>
                                        <th align="center">Valor</th>
                                        <th align="center" colspan="2">Editar</th>
                                    </tr>
                                    <%
                                        List<Produto> produtos = (List<Produto>) request.getAttribute("produtos");
                                        for (Produto produto : produtos) {
                                    %>
                                    <tr>
                                        <td align="center"><%=produto.getIdProduto()%></td>
                                        <td align="center"><%=produto.getDescricaoProduto()%></td>
                                        <td align="center"><%=produto.getMarcaProduto()%></td>
                                        <td align="center"><%=produto.getModeloProduto()%></td>
                                        <td align="center"><%=produto.getValorProduto()%></td>
                                        <td align="center"><a href="ExcluirProduto?idProduto=<%=produto.getIdProduto()%>">Excluir</td>
                                        <td align="center"><a href="CarregarProduto?idProduto=<%=produto.getIdProduto()%>">Alterar</td>
                                    </tr>
                                    <%
                                        }
                                    %>
                                </table>
                                <h3 align="center">${mensagem}</h3>
                            </div>
                            <script>
                                var slideIndex = 1;
                                showDivs(slideIndex);

                                function plusDivs(n) {
                                    showDivs(slideIndex += n);
                                }

                                function showDivs(n) {
                                    var i;
                                    var x = document.getElementsByClassName("mySlides");
                                    if (n > x.length) {
                                        slideIndex = 1
                                    }
                                    if (n < 1) {
                                        slideIndex = x.length
                                    }
                                    for (i = 0; i < x.length; i++) {
                                        x[i].style.display = "none";
                                    }
                                    x[slideIndex - 1].style.display = "block";
                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </body>
</html>
