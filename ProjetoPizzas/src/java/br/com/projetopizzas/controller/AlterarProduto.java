package br.com.projetopizzas.controller;

import br.com.projetopizzas.dao.GenericDAO;
import br.com.projetopizzas.dao.ProdutoDAOImpl;
import br.com.projetopizzas.model.Produto;
import com.sun.xml.rpc.processor.modeler.j2ee.xml.string;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "AlterarProduto", urlPatterns = {"/AlterarProduto"})
public class AlterarProduto extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Integer idProuto = Integer.parseInt(request.getParameter("idProduto"));
        String descProduto = request.getParameter("descricaoProduto");
        String marcaProduto = request.getParameter("marcaProduto");
        String modeloProduto = request.getParameter("modeloProduto");
        Double valorProduto = Double.parseDouble(request.getParameter("valorProduto"));
        String mensagem = null;

        Produto oProduto = new Produto();
        oProduto.setDescricaoProduto(descProduto);
        oProduto.setMarcaProduto(marcaProduto);
        oProduto.setModeloProduto(modeloProduto);
        oProduto.setValorProduto(valorProduto);
        oProduto.setIdProduto(idProuto);

        try {
            GenericDAO dao = new ProdutoDAOImpl();
            if (dao.alterar(oProduto)) {
                mensagem = "Produto alterado com sucesso";

            } else {
                mensagem = "Problemas ao alterar produto."
                        + "Verifique os dados informados e tente novamente.";
            }
            request.setAttribute("mensagem", mensagem);
            request.getRequestDispatcher("ListarProduto").forward(request, response);
        } catch (Exception ex) {
            System.out.println("Problema no servlet ao alterar Produto! Erro:"
                    + ex.getMessage());
            ex.printStackTrace();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
