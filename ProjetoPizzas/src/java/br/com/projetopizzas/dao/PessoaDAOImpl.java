package br.com.projetopizzas.dao;

import br.com.projetopizzas.model.Pessoa;
import br.com.projetopizzas.util.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PessoaDAOImpl implements GenericDAO{
    
    private Connection conn;

    public PessoaDAOImpl() throws Exception {
        try {
            this.conn = ConnectionFactory.getConnection();
            System.out.println("Conectado com sucesso!");
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public Boolean cadastrar(Object object) {
        Pessoa oPessoa = (Pessoa) object;
        PreparedStatement stmt = null;
        
        String sql = "insert into pessoa(nomePessoa, "
                   + "                   cpfPessoa, "
                   + "                   telefonePessoa, "
                   + "                   emailPessoa, "
                   + "                   senhaPessoa) "
                   + "values (?,?,?,?,?);";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, oPessoa.getNomePessoa());
            stmt.setString(2, oPessoa.getCpfPessoa());
            stmt.setString(3, oPessoa.getTelefonePessoa());
            stmt.setString(4, oPessoa.getEmailPessoa());
            stmt.setString(5, oPessoa.getSenhaPessoa());
            stmt.execute();
            return true;
        } catch (Exception ex) {
            System.out.println("Problemas ao cadastrar Pessoa! Erro: " +ex.getMessage());
            ex.printStackTrace();
            return false;
        }finally{
            try {
                ConnectionFactory.closeConnection(conn, stmt);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar a conexão! Erro:" + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    @Override
    public List<Object> listar() {
        List<Object> pessoas = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        String sql = "select * from pessoa;";
        
        try {
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            
            while (rs.next()) {                
                Pessoa oPessoa = new Pessoa();
                oPessoa.setNomePessoa(rs.getString("nomePessoa"));
                oPessoa.setNomePessoa(rs.getString("cpfPessoa"));
                oPessoa.setNomePessoa(rs.getString("telefonePessoa"));
                oPessoa.setNomePessoa(rs.getString("emailPessoa"));
                oPessoa.setNomePessoa(rs.getString("senhaPessoa"));
                pessoas.add(oPessoa);
            }
        } catch (SQLException ex) {
            System.out.println("Problemas ao listar pessoas! Erro: " +ex.getMessage());
            ex.printStackTrace();
        } finally{
            try {
                ConnectionFactory.closeConnection(conn, stmt, rs);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar conexão! Erro:" +ex.getMessage());
                ex.printStackTrace();
            }
        }
        return pessoas;
    }

    @Override
    public Boolean excluir(int idObject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object carregar(int idObject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean alterar(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
