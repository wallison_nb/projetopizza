package br.com.projetopizzas.dao;

import br.com.projetopizzas.model.Produto;
import br.com.projetopizzas.util.ConnectionFactory;
import com.sun.media.sound.DLSModulator;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProdutoDAOImpl implements GenericDAO {
    
    private Connection conn;

    public ProdutoDAOImpl() throws Exception {
        try {
            this.conn = ConnectionFactory.getConnection();
            System.out.println("Conectado com sucesso!");
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public Boolean cadastrar(Object object) {
        Produto oproduto = (Produto) object;
        PreparedStatement stmt = null;

        String sql = "insert into produto(descproduto, marcaproduto, modeloproduto, valorproduto) "
                + "values (?,?,?,?);";

        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, oproduto.getDescricaoProduto());
            stmt.setString(2, oproduto.getMarcaProduto());
            stmt.setString(3, oproduto.getModeloProduto());
            stmt.setDouble(4, oproduto.getValorProduto());
            stmt.execute();
            return true;
        } catch (Exception ex) {
            System.out.println("Problemas ao cadastrar produto! Erro: " + ex.getMessage());
            ex.printStackTrace();
            return false;
        } finally {
            try {
                ConnectionFactory.closeConnection(conn, stmt);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar a conexão! Erro:" + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    @Override
    public List<Object> listar() {
        List<Object> produtos = new ArrayList<>();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        String sql = "select * from produto;";
        
        try {
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            
            while (rs.next()) {                
                Produto oProduto = new Produto();
                oProduto.setIdProduto(rs.getInt("idproduto"));
                oProduto.setDescricaoProduto(rs.getString("descproduto"));
                oProduto.setMarcaProduto(rs.getString("marcaproduto"));
                oProduto.setModeloProduto(rs.getString("modeloproduto"));
                oProduto.setValorProduto(rs.getDouble("valorproduto"));
                produtos.add(oProduto);
            }
        } catch (SQLException ex) {
            System.out.println("Problemas ao listar produtos! Erro: " +ex.getMessage());
            ex.printStackTrace();
        } finally{
            try {
                ConnectionFactory.closeConnection(conn, stmt, rs);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar conexão! Erro:" +ex.getMessage());
                ex.printStackTrace();
            }
        }
        return produtos;
    }

    @Override
    public Boolean excluir(int idObject) {
        PreparedStatement stmt = null;  
        String sql = "Delete from produto where idproduto = ?";
        
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, idObject);
            stmt.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println("Problemas ao excluir produto! Erro: " +ex.getMessage());
            ex.printStackTrace();
            return false;
        }finally{
            try {
                ConnectionFactory.closeConnection(conn, stmt);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar a conexão! Erro: " +ex.getMessage());
                ex.printStackTrace();
            }
        }
        
    }

    @Override
    public Object carregar(int idObject) {  
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        Produto produto = null;
        
        String sql = "Select * from produto where idproduto = ?";
        
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, idObject);
            rs = stmt.executeQuery();
            
            while (rs.next()){
                produto = new Produto();
                produto.setIdProduto(rs.getInt("idproduto"));
                produto.setDescricaoProduto(rs.getString("descproduto"));
                produto.setMarcaProduto(rs.getString("marcaproduto"));
                produto.setModeloProduto(rs.getString("modeloproduto"));
                produto.setValorProduto(rs.getDouble("valorproduto"));
                produto.setValorProduto(rs.getDouble("valorproduto"));
            }
        } catch (SQLException ex) {
            System.out.println("Problemas ao carregar produto! Erro: "+ex.getMessage());
            ex.printStackTrace();
        }finally{
            try {
                ConnectionFactory.closeConnection(conn, stmt, rs);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar conexão! Erro: " +ex.getMessage());
                ex.printStackTrace();
            }
        }
        return produto;
    }

    @Override
    public Boolean alterar(Object object) {
        Produto produto = (Produto) object;
        PreparedStatement stmt = null; 
        
        String sql = "update produto set descproduto = ?, marcaproduto = ?, modeloproduto = ?, valorproduto = ? where idproduto = ?";
        
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, produto.getDescricaoProduto());
            stmt.setString(2, produto.getMarcaProduto());
            stmt.setString(3, produto.getModeloProduto());
            stmt.setDouble(4, produto.getValorProduto());
            stmt.setInt(5, produto.getIdProduto());
            stmt.executeUpdate();
            return true;
        } catch (Exception ex) {
            System.out.println("Problemas ao alterar produto! Erro: " +ex.getMessage());
            ex.printStackTrace();
            return false;
        }finally{
            try {
                ConnectionFactory.closeConnection(conn, stmt);
            } catch (Exception ex) {
                System.out.println("Problemas ao fechar a conexão! Erro: " +ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

}
