<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="bootstrap3/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap3/bootstrap.min.js    " type="text/javascript"></script>
        <script src="bootstrap3/jquery.min.js" type="text/javascript"></script>
        <link href="bootstrap3/css.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">  
        <link href="css.css" rel="stylesheet" type="text/css"/>
        <style>
            /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
            .row.content {height: 550px}

            /* Set gray background color and 100% height */
            .sidenav {
                background-color: #f1f1f1;
                height: 100%;
            }

            /* On small screens, set height to 'auto' for the grid */
            @media screen and (max-width: 767px) {
                .row.content {height: auto;} 
            }
        </style>
        <script>
            function mascara(i) {

                var v = i.value;

                if (isNaN(v[v.length - 1])) { // impede entrar outro caractere que n�o seja n�mero
                    i.value = v.substring(0, v.length - 1);
                    return;
                }

                i.setAttribute("maxlength", "14");
                if (v.length == 3 || v.length == 7)
                    i.value += ".";
                if (v.length == 11)
                    i.value += "-";

            }

            /* M�scaras ER */
            function mascaratelefone(o, f) {
                v_obj = o
                v_fun = f
                setTimeout("execmascaratelefone()", 1)
            }
            function execmascaratelefone() {
                v_obj.value = v_fun(v_obj.value)
            }
            function mtel(v) {
                v = v.replace(/\D/g, ""); //Remove tudo o que n�o � d�gito
                v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca par�nteses em volta dos dois primeiros d�gitos
                v = v.replace(/(\d)(\d{4})$/, "$1-$2"); //Coloca h�fen entre o quarto e o quinto d�gitos
                return v;
            }
            function id(el) {
                return document.getElementById(el);
            }
            window.onload = function () {
                id('telefone').onkeyup = function () {
                    mascaratelefone(this, mtel);
                }
            }
        </script>
    </head>
    <body>
        <form name="cadastrarPessoa" action="CadastrarPessoa" method="post">
            <nav class="navbar navbar-inverse visible-xs">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>                        
                        </button>
                        <a class="navbar-brand" href="#">Logo</a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">Dashboard</a></li>
                            <li><a href="#">Age</a></li>
                            <li><a href="#">Gender</a></li>
                            <li><a href="#">Geo</a></li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="container-fluid">
                <div class="row content">
                    <div class="col-sm-3 sidenav hidden-xs">
                        <h2>
                            <img id="logo" src="img/logo.jpg" alt=""/>
                        </h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="dashboard.jsp">Dashboard</a></li>
                            <li class="active"><a href="#">Cadastro de Clientes</a></li>
                            <li><a href="lanches.jsp">Listar Clientes</a></li>
                            <li><a href="lanches.jsp">Cadastrar Produtos</a></li>
                            <li><a href="lanches.jsp">Listar Produtos</a></li>
                        </ul><br>
                    </div>
                    <br>
                    <div class="col-sm-5">
                        <div class="row">
                            <div class="col-md-6 mx-auto p-0">
                                <div class="card">
                                    <div class="login-box">
                                        <div class="login-snip"> <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-2" class="tab">Cadastrar</label> <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab"></label>
                                            <div class="login-space">
                                                <div class="login">
                                                    <div class="group"> <label for="user" class="label">Nome</label> <input id="user" type="text" class="input" placeholder="Informe seu nome" name="nomePessoa"> </div>
                                                    <div class="group"> <label for="user" class="label">Cpf</label> <input oninput="mascara(this)" id="user" type="text" class="input" placeholder="Informe seu cpf" name="cpfPessoa"> </div>
                                                    <div class="group"> <label for="user" class="label">Telefone</label> <input id="telefone" type="text" class="input" placeholder="Informe seu telefone" name="telefonePessoa" maxlength="15"> </div>
                                                    <div class="group"> <label for="pass" class="label">Email</label> <input id="pass" type="email" class="input" placeholder="Informe o seu email" name="emailPessoa"> </div>
                                                    <div class="group"> <label for="pass" class="label">Senha</label> <input id="pass" type="password" class="input" data-type="password" placeholder="Informe uma senha" name="senhaPessoa"> </div>
                                                    <div class="group"> <input type="submit" class="button" value="Cadastrar" name="cadastrar"> </div>
                                                    <div class="group"><label for="pass" class="label">${mensagem}</label></div>          
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
