<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="bootstrap3/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap3/bootstrap.min.js    " type="text/javascript"></script>
        <script src="bootstrap3/jquery.min.js" type="text/javascript"></script>
        <link href="bootstrap3/css.css" rel="stylesheet" type="text/css"/>

        <link href="bootstrap3/slideshow.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap3/slideshow.js" type="text/javascript"></script>
    </head>
    <body>

        <nav class="navbar navbar-inverse visible-xs">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#">Logo</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Dashboard</a></li>
                        <li><a href="#">Age</a></li>
                        <li><a href="#">Gender</a></li>
                        <li><a href="contato.html">Contato</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row content">
                <div class="col-sm-3 sidenav hidden-xs">
                    <h2>
                        <img id="logo" src="img/logo.jpg" alt=""/>
                    </h2>
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="index.jsp">Pizzas</a></li>
                        <li><a href="lanches.jsp">Lanches</a></li>
                        <li class="active"><a href="doce.jsp">Pizzas Doces</a></li>
                        <li><a href="contato.jsp">Contato</a></li>
                        <li><a href="login.jsp">Entrar</a></li>
                    </ul><br>
                </div>
                <br>

                <div class="col-sm-9">
                    <div class="well">
                        <div class="container">
                            <h2 class="texto">Todas as Pizzas Doces</h2>  
                            <div class="w3-content w3-display-container">
                                <img class="mySlides" src="img/doce1.png" style="width:100%">
                                <img class="mySlides" src="img/doce2.png" style="width:100%">
                                <img class="mySlides" src="img/doce3.png" style="width:100%">

                                <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
                                <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
                            </div>
                            <script>
                                var slideIndex = 1;
                                showDivs(slideIndex);

                                function plusDivs(n) {
                                    showDivs(slideIndex += n);
                                }

                                function showDivs(n) {
                                    var i;
                                    var x = document.getElementsByClassName("mySlides");
                                    if (n > x.length) {
                                        slideIndex = 1
                                    }
                                    if (n < 1) {
                                        slideIndex = x.length
                                    }
                                    for (i = 0; i < x.length; i++) {
                                        x[i].style.display = "none";
                                    }
                                    x[slideIndex - 1].style.display = "block";
                                }
                            </script>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/banana.png" alt=""/>
                                <h4>Banana:</h4>
                                <p>Mussarela, banana, a�ucar com canela e leite condensado</p>
                                <h4>R$ 38,50</h4>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/doce2.png" alt=""/>
                                <h4>Choconana:</h4>
                                <p>Mussarela, banana, a�ucar com canela e chocolate</p>
                                <h4>R$ 42,00</h4>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/bacaxi.png" alt=""/>
                                <h4>Abacaxi:</h4>
                                <p>Mussarela, abacaxi, a�ucar com canela e leite condensado</p>
                                <h4>R$ 39,00</h4>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/doce1.png" alt=""/>
                                <h4>Mista:</h4>
                                <p>Mussarela, banana, abacaxi, a�ucar com canela e leite condensado</p>
                                <h4>R$ 37,50</h4>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/morango.png" alt=""/>
                                <h4>Morango:</h4>
                                <p>Chocolate branco ao forno e calda de morango</p>
                                <h4>R$ 40,00</h4>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/milho.png" alt=""/>
                                <h4>Beijinho:</h4>
                                <p>Chocolate branco ao forno, coberto com coco ralad</p>
                                <h4>R$ 39,00</h4>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/doce1.png" alt=""/>
                                <h4>Prest�gio:</h4>
                                <p>Chocolate escuro ao forno coberto com coco ralado</p>
                                <h4>R$ 40,00</h4>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/doce2.png" alt=""/>
                                <h4>Brigadeiro:</h4>
                                <p>Chocolate escuro ao forno coberto de chocolate granulado</p>
                                <h4>R$ 38,50</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </body>
</html>
