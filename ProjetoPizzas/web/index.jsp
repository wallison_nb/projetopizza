<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="bootstrap3/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap3/bootstrap.min.js    " type="text/javascript"></script>
        <script src="bootstrap3/jquery.min.js" type="text/javascript"></script>
        <link href="bootstrap3/css.css" rel="stylesheet" type="text/css"/>
        <link href="bootstrap3/slideshow.css" rel="stylesheet" type="text/css"/>
        <script src="bootstrap3/slideshow.js" type="text/javascript"></script>
    </head>
    <body>
        <nav class="navbar navbar-inverse visible-xs">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#">Logo</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Dashboard</a></li>
                        <li><a href="#">Age</a></li>
                        <li><a href="#">Gender</a></li>
                        <li><a href="contato.jsp">Contato</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row content">
                <div class="col-sm-3 sidenav hidden-xs">
                    <h2>
                        <img id="logo" src="img/logo.jpg" alt=""/>
                    </h2>
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active"><a href="#section1">Pizzas</a></li>
                        <li><a href="lanches.jsp">Lanches</a></li>
                        <li><a href="doce.jsp">Pizzas Doces</a></li>
                        <li><a href="contato.jsp">Contato</a></li>
                        <li><a href="login.jsp">Entrar</a></li>
                    </ul><br>
                </div>
                <br>

                <div class="col-sm-9">
                    <div class="well">
                        <div class="container">
                            <h2 class="texto">Todas as Pizzas</h2>  
                            <div class="w3-content w3-display-container">
                                <img class="mySlides" src="img/pizza.jpg" style="width:100%">
                                <img class="mySlides" src="img/pizza2.jpg" style="width:100%">
                                <img class="mySlides" src="img/pizza3.jpg" style="width:100%">

                                <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
                                <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
                            </div>
                            <script>
                                var slideIndex = 1;
                                showDivs(slideIndex);

                                function plusDivs(n) {
                                    showDivs(slideIndex += n);
                                }

                                function showDivs(n) {
                                    var i;
                                    var x = document.getElementsByClassName("mySlides");
                                    if (n > x.length) {
                                        slideIndex = 1
                                    }
                                    if (n < 1) {
                                        slideIndex = x.length
                                    }
                                    for (i = 0; i < x.length; i++) {
                                        x[i].style.display = "none";
                                    }
                                    x[slideIndex - 1].style.display = "block";
                                }
                            </script>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/calabresa.jpg" alt=""/>
                                <h4>Calabresa:</h4>
                                <p>Mussarela, calabresa, cebola, molho, azeitona e or�gano</p>
                                <h4>R$ 37,50</h4>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/frango catupiri.png" alt=""/>
                                <h4>Frango com Catupiri:</h4>
                                <p>Mussarela, frango, catupiry, molho, tomate, azeitona e or�gano</p>
                                <h4>R$ 38,50</h4>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/mussarela.png" alt=""/>
                                <h4>Mussarela:</h4>
                                <p>Mussarela, tomate, molho, azeitona e or�gano</p>
                                <h4>R$ 37,00</h4>
                                <BR>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/vegetariana.png" alt=""/>
                                <h4>Vegetariana:</h4>
                                <p>Mussarela, milho, br�colis, piment�o, palmito, molho, tomate, azeitona e or�gano</p>
                                <h4>R$ 37,50</h4>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/palmito.png" alt=""/>
                                <h4>Palmito:</h4>
                                <p>Mussarela, palmito, tomate, azeitona, molho e or�gano</p>
                                <h4>R$ 37,00</h4>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/milho.png" alt=""/>
                                <h4>Milho:</h4>
                                <p>Mussarela, milho, catupiry, molho, azeitona e or�gano</p>
                                <h4>R$ 37,00</h4>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/bacon.png" alt=""/>
                                <h4>Franbacon:</h4>
                                <p>Mussarela, frango, bacon, molho, tomate e or�gano</p>
                                <h4>R$ 40,00</h4>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/quatroqueijo.png" alt=""/>
                                <h4>Quatro Queijos:</h4>
                                <p>Mussarela, catupiry, parmes�o, cheddar, tomate, molho e azeitona</p>
                                <h4>R$ 38,50</h4>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/catuperesa.png" alt=""/>
                                <h4>Catuperesa:</h4>
                                <p>Mussarela, calabresa, catupiry, molho, azeitona e or�gano</p>
                                <h4>R$ 39,00</h4>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/frango catupiri.png" alt=""/>
                                <h4>Bacon especial:</h4>
                                <p>Mussarela, bacon, cheddar, batata palha, molho, azeitona e or�gano</p>
                                <h4>R$ 39,50</h4>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/calabresa.jpg" alt=""/>
                                <h4>Lombo:</h4>
                                <p>Mussarela, lombo, cebola, molho, azeitona e or�gano</p>
                                <h4>R$ 40,00</h4>
                                <br>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="well">
                                <img id="imagem" src="img/frango catupiri.png" alt=""/>
                                <h4>Portuguesa:</h4>
                                <p>Mussarela, presunto, milho, ervilha, ovo, cebola, tomate, molho e or�gano</p>
                                <h4>R$ 42,00</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </body>
</html>
